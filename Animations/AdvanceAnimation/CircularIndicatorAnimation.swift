//
//  AdvanceAnimation.swift
//  Animations
//
//  Created by shreejwal giri on 27/05/2021.
//

import SwiftUI

struct AdvanceViewAnimation: View {
    @State var percent: CGFloat = 0
    var body: some View {
        VStack {
            Spacer()
            Color.clear.overlay(Indicator(percentage: percent))
            Spacer()
            HStack(spacing: 10) {
                MyButton(label: "0%", font: .headline) { withAnimation(.easeInOut(duration: 1.0)) { self.percent = 0.0 } }
                
                MyButton(label: "50%", font: .headline) { withAnimation(.easeInOut(duration: 1.0)) { self.percent = 0.5 } }
                
                MyButton(label: "90%", font: .headline) { withAnimation(.easeInOut(duration: 1.0)) { self.percent = 0.9 } }
                
                MyButton(label: "27%", font: .headline) { withAnimation(.easeInOut(duration: 1.0)) { self.percent = 0.27 } }
                
                MyButton(label: "100%", font: .headline) { withAnimation(.easeInOut(duration: 1.0)) { self.percent = 1.0 } }
            }
        }
    }
}

struct MyButton: View {
    let label: String
    var font: Font = .title
    var textColor: Color = .white
    let action: () -> ()
    
    var body: some View {
        Button(action: {
            self.action()
        }, label: {
            Text(label)
                .font(font)
                .padding(10)
                .frame(width: 70)
                .background(RoundedRectangle(cornerRadius: 10).foregroundColor(Color.green).shadow(radius: 2))
                .foregroundColor(textColor)
            
        })
    }
}

struct Indicator: View {
    var percentage: CGFloat
    var body: some View {
        Circle()
            .fill(LinearGradient(gradient:  Gradient(colors: [.blue, .purple]), startPoint: .topLeading, endPoint: .bottomTrailing))
            .frame(width: 150, height: 150)
            .modifier(PercentageIndicator(percentage: percentage))
    }
}

struct PercentageIndicator: AnimatableModifier {
    
    var percentage: CGFloat = 0
    
    var animatableData: CGFloat {
        get { percentage }
        set { percentage = newValue }
    }
    
    func body(content: Content) -> some View {
        content
            .overlay(
                ArchShape(percentage: percentage)
                    .foregroundColor(.red)
            )
            .overlay(
                PercentageLabel(percentage: percentage)
            )
    }
    
    struct ArchShape: Shape {
        let percentage: CGFloat
        
        func path(in rect: CGRect) -> Path {
            
            var shapePath = Path()
            shapePath.addArc(
                center: CGPoint(x: rect.width / 2.0, y: rect.height / 2.0),
                radius: rect.height / 2.0 + 15,
                startAngle: .degrees(0),
                endAngle: .degrees(360.0 * Double(percentage)),
                clockwise: true
            )

            return shapePath.strokedPath(.init(lineWidth: 10,dash: [3,6], dashPhase: 10))
        }
    }
    
    struct PercentageLabel: View {
        let percentage: CGFloat
        
        var body: some View {
            Text("\(Int(percentage) * 100)%")
                .font(.largeTitle)
                .fontWeight(/*@START_MENU_TOKEN@*/.bold/*@END_MENU_TOKEN@*/)
                .foregroundColor(.white)
        }
    }
}



struct AdvanceAnimation_Previews: PreviewProvider {
    static var previews: some View {
        AdvanceViewAnimation()
    }
}
