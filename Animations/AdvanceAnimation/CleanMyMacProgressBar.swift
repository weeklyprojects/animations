//
//  CleanMyMacProgressBar.swift
//  Animations
//
//  Created by shreejwal giri on 28/05/2021.
//

import SwiftUI

struct CleanMyMacProgressBar: View {
    @State var buttonTitle: String = "Scan"
    @State var isProgressTriggered: Bool = false
    @State var percentage: CGFloat = 0
    var body: some View {
        ZStack {
            CMMCircleShape(buttonTitle: buttonTitle, percentage: percentage)
        }
        .onTapGesture {
            isProgressTriggered.toggle()
            if isProgressTriggered {
                buttonTitle = "Scan"
                percentage = 0
            } else {
                withAnimation(.easeInOut(duration: 10), {
                    buttonTitle = "Stop"
                    percentage = 360
                })
            }
        }
        
    }
}

struct CMMCircleShape: View {
    let gradientColor: [Color] = [Color(#colorLiteral(red: 0.2780193985, green: 0.2855525017, blue: 0.3503785133, alpha: 1)), Color(#colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1))]
    var buttonTitle: String
    var percentage: CGFloat
    
    var body: some View {
        Circle()
            .fill(
                LinearGradient(
                    gradient: Gradient(colors: gradientColor),
                    startPoint: .bottomTrailing,
                    endPoint: .topLeading)
            )
            .frame(width: 150, height: 150, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
            .modifier(CMMCircleProgressBar(barTitle: buttonTitle, percentage: percentage))
    }
}

struct CMMCircleProgressBar: AnimatableModifier {
    
    var barTitle: String
    var percentage: CGFloat
    
    var animatableData: CGFloat {
        get { percentage }
        set { percentage = newValue }
    }
    
    func body(content: Content) -> some View {
        return content
            .overlay(CircularDefaultArc(barTitle: barTitle, percentage: percentage).foregroundColor(barTitle == "Scan" ? .white : .blue))
            .overlay(SetTitle(barTitle: barTitle, percentage: percentage))
    }
    
    struct CircularDefaultArc: Shape {
        var barTitle: String
        var percentage: CGFloat
        func path(in rect: CGRect) -> Path {
            var shapePath = Path()
            shapePath.addArc(
                center: CGPoint(x: rect.width / 2, y: rect.height / 2),
                radius: (rect.width / 2) - 5,
                startAngle: .degrees(0),
                endAngle: .degrees(barTitle.lowercased() == "scan" ? 0 : Double(percentage)),
                clockwise: false)
            return shapePath.strokedPath(.init(lineWidth: 5, lineCap: .round))
        }
    }
    
    struct SetTitle: View {
        var barTitle: String
        var percentage: CGFloat
        var body: some View {
            VStack {
                Text(barTitle)
                    .fontWeight(.semibold)
                    .foregroundColor(.white)
                    .font(.system(size: 25))
//                Text("\(Int(percentage) / 360 * 100)%")
//                    .fontWeight(.light)
//                    .foregroundColor(.white)
//                    .font(.system(size: 15))
            }
       
        }
    }
}

struct CleanMyMacProgressBar_Previews: PreviewProvider {
    static var previews: some View {
        CleanMyMacProgressBar()
    }
}
