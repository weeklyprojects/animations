//
//  CircularIndicatorAnimationTwo.swift
//  Animations
//
//  Created by shreejwal giri on 27/05/2021.
//

import SwiftUI

struct CircularIndicatorAnimationTwo: View {
    @State var percentage: CGFloat = 0.5
    @State var expand: Bool = false
    var body: some View {
        VStack {
            Spacer()
            Color.clear.overlay(CircularProgressBar(percentage: percentage))
    
            HStack(alignment: .center, spacing: 10, content: {
                CICustomButton(action: {
                    withAnimation(.easeInOut(duration: 2), {
                        percentage = 0.23
                    })
                }, title: "23%")
                CICustomButton(action: {
                    withAnimation(.easeInOut(duration: 2), {
                        percentage = 0.55
                    })
                }, title: "55%")
                CICustomButton(action: {
                    withAnimation(.easeInOut(duration: 2), {
                        percentage = 0.30
                    })
                }, title: "30%")
                CICustomButton(action: {
                    withAnimation(.easeInOut(duration: 2), {
                       expand = true
                    })
                }, title: "exp")
            })
        }
    }
}

struct CICustomButton: View {
    @State var action: (() -> Void)
    @State var title: String
    var body: some View {
        Button(action: { action() }, label: {
            Text("\(title)")
                .foregroundColor(.black)
                .fontWeight(.semibold)
                .background(
                    RoundedRectangle(cornerRadius: 5)
                        .fill(
                            LinearGradient(
                                gradient: Gradient(colors: [Color.gray, Color.white]),
                                startPoint: /*@START_MENU_TOKEN@*/.leading/*@END_MENU_TOKEN@*/,
                                endPoint: /*@START_MENU_TOKEN@*/.trailing/*@END_MENU_TOKEN@*/),
                            style: /*@START_MENU_TOKEN@*/FillStyle()/*@END_MENU_TOKEN@*/
                        )
                        
                        .frame(width: 70, height: 40, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                    , alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
        })
        .frame(width: 70, height: 40, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
        .shadow(radius: 2)
    }
}

struct CircularProgressBar: View {
    var percentage: CGFloat
    @State var expand: Bool = false
    var body: some View {
        Circle()
            .fill(
                LinearGradient(
                    gradient: Gradient(colors: [Color.white, Color.gray]),
                    startPoint: .topLeading,
                    endPoint: .bottomTrailing
                )
            )
            .frame(width: 150, height: 150, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
            .modifier(ProgressLine(percentage: percentage))
            .scaleEffect(expand ? 4 : 1)
    }
}

struct ProgressLine: AnimatableModifier {
    var percentage: CGFloat
    
    var animatableData: CGFloat {
        get { percentage }
        set { percentage = newValue }
    }
    
    func body(content: Content) -> some View {
        content
            .overlay(progressShape(percentage: percentage).foregroundColor(Color.blue))
            .overlay(ProgressTitle(percentage: percentage))
    }
    
    struct progressShape: Shape {
        let percentage: CGFloat
        let rotationAdjustment = Angle.degrees(90)
      
        func path(in rect: CGRect) -> Path {
            let startPoint = rotationAdjustment
            let endPoint = Angle.degrees(360 * Double(percentage)) - rotationAdjustment
            var shapePath = Path()
            shapePath.addArc(
                center: CGPoint(x: rect.width / 2.0, y: rect.height / 2.0),
                radius: rect.height / 2.0 + 8,
                startAngle: startPoint,
                endAngle: endPoint,
                clockwise: true
            )
            return shapePath
                .strokedPath(StrokeStyle(lineWidth: 5, lineCap: .round))
        }
    }
    
    struct ProgressTitle: View {
        var percentage: CGFloat
        var body: some View {
            Text("\(Int(percentage) * 100)%")
                .fontWeight(.bold)
                .font(.title3)
                .foregroundColor(.black)
        }
    }
}

struct CircularIndicatorAnimationTwo_Previews: PreviewProvider {
    static var previews: some View {
        CircularIndicatorAnimationTwo()
    }
}
