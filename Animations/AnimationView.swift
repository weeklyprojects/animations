//
//  AnimationView.swift
//  Animations
//
//  Created by shreejwal giri on 27/05/2021.
//

import SwiftUI

struct AnimationView: View {
    var body: some View {
        VStack {
            ButtonFadeOut()
            ButtonMove()
            Spacer(minLength: 50)
            ButtonAndCircleFlip()
        }
    }
}

// fade out button animation, example of implicit animaiton
struct ButtonFadeOut: View {
    @State var isHidden: Bool = false
    var body: some View {
        Button(action: {
            isHidden.toggle()
        }, label: {
            Text("Click Me")
        })
        .opacity(isHidden ? 0 : 1)
        .animation(.easeInOut(duration: 2))
    }
}

// move button position up and down, example of explicit animation
struct ButtonMove: View {
    @State var changeOffset: Bool = false
    var body: some View {
        Button(action: {
            withAnimation(.interpolatingSpring(mass: 1, stiffness: 80, damping: 4, initialVelocity: 0), {
                changeOffset.toggle()
            })
        }, label: {
            Text("Click Me")
        })
        .offset(y: changeOffset ? 0 : 50)
    }
}

// use of match geometry effect, explicit animaiton
struct ButtonAndCircleFlip: View {
    
    @State     private var flipTheViewsHorizontal: Bool = false
    @State     private var flipTheViewsVertical: Bool = false
    @Namespace private var animationHorizontal
    @Namespace private var animationvertical
    
    var width: CGFloat = 80
    var height: CGFloat = 80

    var body: some View {
        ZStack {
            HStack {
                if flipTheViewsHorizontal {
                   Circle()
                    .fill(Color.green)
                    .frame(width: width, height: height, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                    .matchedGeometryEffect(id: "circleGreen", in: animationHorizontal)
                    Circle()
                        .fill(Color.blue)
                        .frame(width: width, height: height, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                        .matchedGeometryEffect(id: "circleBlue", in: animationHorizontal)
                } else {
                    Circle()
                        .fill(Color.blue)
                        .frame(width: width, height: height, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                        .matchedGeometryEffect(id: "circleBlue", in: animationHorizontal)
                    Circle()
                        .fill(Color.green)
                        .frame(width: width, height: height, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                        .matchedGeometryEffect(id: "circleGreen", in: animationHorizontal)
                }
            }
            
            VStack {
                if flipTheViewsVertical {
                    Circle()
                        .fill(Color.purple)
                        .frame(width: width, height: height, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                        .matchedGeometryEffect(id: "circlePurple", in: animationvertical)
                    Circle()
                        .fill(Color.yellow)
                        .frame(width: width, height: height, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                        .matchedGeometryEffect(id: "circleYellow", in: animationvertical)
                } else {
                    Circle()
                        .fill(Color.yellow)
                        .frame(width: width, height: height, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                        .matchedGeometryEffect(id: "circleYellow", in: animationvertical)
                    Circle()
                        .fill(Color.purple)
                        .frame(width: width, height: height, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                        .matchedGeometryEffect(id: "circlePurple", in: animationvertical)
                }
            }
        }
        .onTapGesture {
            withAnimation{
                flipTheViewsHorizontal.toggle()
                flipTheViewsVertical.toggle()
            }
        }
    }
}

struct AnimationView_Previews: PreviewProvider {
    static var previews: some View {
        AnimationView()
    }
}
