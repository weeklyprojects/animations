//
//  AdvanceShape.swift
//  Animations
//
//  Created by shreejwal giri on 27/05/2021.
//

import SwiftUI

struct AdvanceShape: View {
    
    var body: some View {
        ZStack {
           ASLine(pt1: CGPoint(x: 100, y: 100), length: 500, direction: Angle(degrees: 25))
            .stroke(Color.blue, lineWidth: 3)
        }
    }
}

struct ASLine: Shape {
    let pt1: CGPoint
    let length: CGFloat
    let direction: Angle
    
    func path(in rect: CGRect) -> Path {
        let x = pt1.x + length * CGFloat(cos(direction.radians))
        let y = pt1.y + length * CGFloat(sin(direction.radians))
        let pt2 = CGPoint(x: x, y: y)
        var path = Path()
        path.move(to: pt1)
        path.addLine(to: pt2)
        return path
    }
}

struct AdvanceShape_Previews: PreviewProvider {
    static var previews: some View {
        AdvanceShape()
    }
}
